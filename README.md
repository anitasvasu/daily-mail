# daily-mail

Sends a daily mail to your favourite person by picking an item from a text file. 
Designed for MacOS

## Setup msmtp
```
brew install msmtp
```

Download https://pki.goog/roots.pem and
keep in ~/ssl-certs folder. These are the certificates you need to trust
gmail SMTP server. (Do not use Thawte or Equifax certificates!)

```
mkdir ~/ssl-certs
curl https://pki.goog/roots.pem > ~/ssl-certs/roots.pem
```

Update your `~/.msmtprc` file, replace user@gmail.com with your own gmail
email.

```
# Begin msmtprc 
# Set default values for all following accounts.
defaults
tls on
logfile ~/.msmtp.log

account user@gmail.com
host smtp.gmail.com
port 587
protocol smtp
auth on
from user@gmail.com
user user@gmail.com
tls on
tls_starttls on

tls_trust_file ~/ssl-certs/roots.pem

# Set a default account
# You need to set a default account for Mail
account default : user@gmail.com

# end msmtprc

```

Now open Key Chain Access and make a new entry:
```
Name: smtp.gmail.com
Account: user@gmail.com
Where: smtp://smtp.gmail.com
```
With your gmail id as account and password as your password. If you are using second factor
authentication generate an app password first. This helps you keep track of which password
is used for which application and avoids second factor.

Now write a source folder, which will act as DB for this application. In my case ~/top10

Make a file called `~/top10/items.txt`
```
Subject1
Content1
Subject2
Content2
```

Make a mailer.sh file as follows,
```
cd ~/top10
head -2 items.txt > pick.txt
SUBJECT=$(head -1 pick.txt)
CONTENT=$(tail -1 pick.txt)
echo "${CONTENT}" | mail -s "${SUBJECT}" -c user@gmail.com recipient@gmail.com
SNAPSHOT_ITEMS="items-${RANDOM}-snapshot.txt"
cp items.txt ${SNAPSHOT_ITEMS}
sed "1,2d" ${SNAPSHOT_ITEMS} > items.txt
```

Apart from sending the mail, it will also create a snapshot file for items in case something breaks like
mail fails, in which case, you can restore it by copying the snapshot file back to items.txt. And
remember to periodically delete snapshot files to avoid clutter after verifying all mails have been
correctly sent. Since the mail is CC'd to your own email address it will be easier to ensure it has been
sent.

```
rm *-snapshot.txt
```

Now setup crontab to ensure the mails are send regularly, say every day morning at 6 AM. To
use cron type `crontab -e`
```
0 6 * * * /Users/anitvasu/top10/mailer.sh
```








